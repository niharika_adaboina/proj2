The violin, also known informally as a fiddle, is a wooden string instrument in the violin family. Most violins have a hollow wooden body. It is the smallest and highest-pitched instrument in the family in regular use. Smaller violin-type instruments are known, including the violino piccolo and the kit violin, but these are virtually unused. The violin typically has four strings tuned in perfect fifths, and is most commonly played by drawing a bow across its strings, though it can also be played by plucking the strings with the fingers (pizzicato) and by striking the strings with the wooden side of the bow (col legno).

Violins are important instruments in a wide variety of musical genres. They are most prominent in the Western classical tradition, both in ensembles (from chamber music to orchestras) and as solo instruments and in many varieties of folk music, including country music, bluegrass music and in jazz. Electric violins with solid bodies and piezoelectric pickups are used in some forms of rock music and jazz fusion, with the pickups plugged into instrument amplifiers and speakers to produce sound. Further, the violin has come to be played in many non-Western music cultures, including Indian music and Iranian music. The name fiddle is often used regardless of the type of music played on it.

The violin was first known in 16th-century Italy, with some further modifications occurring in the 18th and 19th centuries to give the instrument a more powerful sound and projection. In Europe, it served as the basis for the development of other stringed instruments used in Western classical music, such as the viola.[1][2][3]

Violinists and collectors particularly prize the fine historical instruments made by the Stradivari, Guarneri and Amati families from the 16th to the 18th century in Brescia and Cremona (Italy) and by Jacob Stainer in Austria. According to their reputation, the quality of their sound has defied attempts to explain or equal it, though this belief is disputed.[4][5] Great numbers of instruments have come from the hands of less famous makers, as well as still greater numbers of mass-produced commercial "trade violins" coming from cottage industries in places such as Saxony, Bohemia, and Mirecourt. Many of these trade instruments were formerly sold by Sears, Roebuck and Co. and other mass merchandisers.







**************
++++++++++++++
--------------


1234567890



